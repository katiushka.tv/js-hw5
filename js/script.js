//"use strict"
function createNewUser() {
    let userName = prompt("Enter your name, please.");
    let userLastName = prompt("Enter your last name");
    return  {
        firstName: userName,
        lastName: userLastName,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        }
    }
}
const newUser = createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
//--------------optional task-----------------
Object.defineProperties(newUser, {
    firstName: {
        writable: false,
    },
    lastName: {
        writable: false,
    },
});
function setFirstName(firstName) {
    Object.defineProperties(newUser, {
        firstName: {
            value: firstName,
        },
    });
}
function setLastName(lastName) {
    Object.defineProperties(newUser, {
        lastName: {
            value: lastName,
        },
    });
}
//-----------checkup------------
newUser.firstName = "Ivan";
newUser.lastName = "Ivanov";
console.log(newUser);
setFirstName("Pavlo");
setLastName("Holub");
console.log(newUser);